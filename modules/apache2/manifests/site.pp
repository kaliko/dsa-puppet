# Install and enable an apache site
#
# @param source  source of the apache vhost file
# @param content content of the apache vhost file
# @param ensure  present or absent
# @param site    site name
define apache2::site (
  Optional[String] $source = undef,
  Optional[String] $content = undef,
  Enum['present','absent'] $ensure = 'present',
  String $site = $name
) {
  include apache2

  if $ensure == 'present' {
    if ! ($source or $content) {
      fail ( "No configuration (source or content) found for ${name}" )
    }
  }

  $target = "/etc/apache2/sites-available/${site}"
  $symlink = "/etc/apache2/sites-enabled/${name}.conf"
  $link_ensure = $ensure ? {
    present => link,
    absent  => absent,
  }

  file { $target:
    ensure  => $ensure,
    content => $content,
    source  => $source,
    require => Package['apache2'],
    notify  => Exec['service apache2 reload'],
  }
  file { $symlink:
    ensure => $link_ensure,
    target => $target,
    notify => Exec['service apache2 reload'],
  }
}
