# enable (or disable) lingering for a user
define dsa_systemd::linger(
  Enum['present','absent'] $ensure = 'present',
) {
  include dsa_systemd

  $command = $ensure ? {
    present => 'enable-linger',
    absent  => 'disable-linger',
  }

  exec { "${command} for user ${name}":
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => "loginctl ${command} ${name}",
    refreshonly => true,
  }

  file { "/var/lib/systemd/linger/${name}":
    ensure => $ensure,
    notify => Exec["${command} for user ${name}"],
  }
}
