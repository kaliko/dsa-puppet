# ssh fail2ban setup
#
class fail2ban::ssh inherits fail2ban {
	# currently using the fail2ban-provided SSH filter configuration
	file { '/etc/fail2ban/jail.d/dsa-ssh.conf':
		source  => 'puppet:///modules/fail2ban/jail/dsa-ssh.conf',
		notify  => Service['fail2ban'],
		require => Package['fail2ban'],
	}
}
