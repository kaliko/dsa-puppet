# the bacula storage node
#
# @param backup_path      directory where backups should be stored
# @param filestor_device  storage device name prefix
# @param filestor_name    storage device media type name prefix
# @param port_sd          port for the sd to listen on
# @param storage_name     bacula name of this sd instance
# @param storage_address  address of this sd instance that other instances should connect to (dns name)
class bacula::storage (
  String $backup_path     = '/srv/bacula',
  String $filestor_device = 'FileStorage',
  String $filestor_name   = 'File',
  Integer $port_sd        = 9103,
  String $storage_name    = "${::fqdn}-sd",
  Stdlib::Host $storage_address = $::fqdn,
) inherits bacula {

  package { 'bacula-sd':
    ensure => installed
  }

  service { 'bacula-sd':
    ensure    => running,
    enable    => true,
    hasstatus => true,
  }
  dsa_systemd::override { 'bacula-sd':
    content => @(EOT)
      [Unit]
      After=network-online.target unbound.service
      [Service]
      Group=bacula
      SupplementaryGroups=ssl-cert
      | EOT
  }
  file { "${backup_path}/.nobackup":
    content => '',
  }

  exec { 'bacula-sd restart-when-idle':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => "sh -c 'setsid /usr/local/sbin/bacula-idle-restart ${port_sd} bacula-sd &'",
    refreshonly => true,
    subscribe   => File[$bacula::ssl_server_cert],
    require     => File['/usr/local/sbin/bacula-idle-restart'],
  }


  file { '/etc/bacula/bacula-sd.conf':
    content => template('bacula/bacula-sd.conf.erb'),
    mode    => '0640',
    group   => bacula,
    notify  => Exec['bacula-sd restart-when-idle']
  }

  file { '/etc/bacula/storage-conf.d':
    ensure  => directory,
    mode    => '0755',
    group   => bacula,
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec['bacula-sd restart-when-idle']
  }

  # allow access from director and fds
  ferm::rule::simple { 'dsa-bacula-sd':
    description => 'Access to the bacula-storage',
    port        => $port_sd,
    target      => 'bacula-sd',
  }
  Ferm::Rule::Simple <<| tag == 'bacula::director-to-storage' |>>;
  Ferm::Rule::Simple <<| tag == "bacula::fd-to-storage::${::fqdn}" |>>;

  file { '/etc/bacula/storage-conf.d/empty.conf':
    content => '',
    mode    => '0440',
    group   => bacula,
    notify  => Exec['bacula-sd restart-when-idle']
  }

  package { 'python3-psycopg2': ensure => installed }
  file { '/usr/local/bin/bacula-unlink-removed-volumes':
    source => 'puppet:///modules/bacula/bacula-unlink-removed-volumes',
    mode   => '0555',
  }
  file { '/etc/cron.d/puppet-bacula-storage-stuff': ensure => absent, }
  concat::fragment { 'puppet-crontab--bacula-storage':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      @daily bacula chronic /usr/local/bin/bacula-unlink-removed-volumes -v
      | EOF
  }

  Bacula::Storage::Director<<| tag == 'bacula::to-storage' |>>
  Bacula::Storage::Client<<| tag == "bacula::to-storage::${::fqdn}" |>>
}
