# = Class: spamassassin
#
# Manage a spamassassin installation
#
# == Sample Usage:
#
#   include spamassassin
#
class spamassassin {
  package { [
      'spamassassin',
      'spamc',
    ]:
      ensure => installed
  }

  service { 'spamassassin':
    ensure  => running,
    require => Package['spamassassin']
  }

  file { '/usr/share/munin/plugins/spamassassin':
    ensure => file,
    source => 'puppet:///modules/spamassassin/munin/spamassassin',
    mode   => '0755',
  }

  munin::check { 'spamassassin': }
}
