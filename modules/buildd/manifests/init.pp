# A debian.org buildd
#
# @param buildd_tool
#   Override the default (currently buildd) on whether to use pybuildd or the
#   the classic buildd.
class buildd (
  Enum['buildd', 'pybuildd'] $buildd_tool = 'buildd',
) {
  # Do nothing until we get the buildd user from ldap
  if $facts['buildd_user_exists'] {
    # home directory
    file { '/home/buildd':
      ensure => directory,
      mode   => '2755',
      group  => buildd,
      owner  => buildd,
    }

    include buildd::sbuild
    include buildd::dupload
    include buildd::aptitude
    include buildd::gnupg
    include buildd::ssh

    include "buildd::${buildd_tool}"
  }

  if $facts['debarchitecture'] == 'arm64' {
    # Enable SETEND and SWP instructions emulation in aarch32 compat mode,
    # needed to support armel binaries.
    #
    # Use hardware implementation of CP15 Barriers. The default value (1)
    # emulates the instructions in software and causes *lots* of messages to be
    # logged to syslog.
    debian_org::sysctl::value {
      'abi.setend'      : value => '1';
      'abi.swp'         : value => '1';
      'abi.cp15_barrier': value => '2';
    }
  }
}
