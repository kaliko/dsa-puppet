# NFS server checks
class dsa::debmon::check_nfs_server (
) {
  $expect_nfs_server = defined(Class['roles::nfs_server'])

  if $expect_nfs_server {
    mon::service { 'procs/nfsd': # {{{
      vars => {
        argument => '[nfsd]',
        command  => 'nfsd',
        user     => 'root',
        warning  => '1:10',
        critical => '1:',
      }
    } # }}}
    mon::service { 'procs/lockd': # {{{
      vars => {
        argument => '[lockd]',
        command  => 'lockd',
        user     => 'root',
        warning  => '1:1',
        critical => '1:',
      }
    } # }}}
    mon::service { 'procs/rpc.mountd': # {{{
      vars => {
        argument => '/sbin/rpc.mountd',
        command  => 'rpc.mountd',
        user     => 'root',
        warning  => '1:1',
        critical => '1:',
      }
    } # }}}
  } else {
    # unwanted processes
    [
      'nfsd',
      'lockd',
      'rpc.mountd',
    ].each |$command| {
      mon::service { "procs/unwanted-process-${command}":
        check_interval => '60m',
        retry_interval => '15m',
        vars           => {
          command => $command,
          warning => '0:0'
        },
      }
    }
  }
}
# vim:set fdm=marker:
