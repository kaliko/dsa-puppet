# Set up (at least monitoring for) queued
class dsa::profile::queued {
  mon::service { 'procs/debianqueued': # {{{
    vars => {
      command  => 'debianqueued',
      user     => 'dak-unpriv',
      warning  => '1:3',
      critical => '1:',
    }
  } # }}}
}
