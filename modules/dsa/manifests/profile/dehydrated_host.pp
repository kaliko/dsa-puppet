# The central host that does the dehydrated job for the
# dehydrated puppet class.  This just sets up the hook script
# called by the dehydrated puppet module and its config.
#
# See the documentation of that module for further details.
class dsa::profile::dehydrated_host {
  include dehydrated

  dsa::util::sudo { 'puppet-dehydrated-update-domains':
    user    => 'dehydrated',
    run_as  => 'dnsadm',
    command => '/srv/dns.debian.org/bin/update --no-keys-update',
  }

  file {
    [
      '/srv/dehydrated',
      '/srv/dehydrated/var',
      '/srv/dehydrated/var/hook',
    ]:
      ensure => directory,
      owner  => 'dehydrated',
      group  => 'dehydrated',
      mode   => '0755',
  }

  # We actually use the same hook script as our letsencrypt-domains
  # setup.  So just symlink things for now.  If (When!) we
  # retire letsencrypt-domains, we may want to copy just this script.
  Class['dehydrated::setup::dehydrated_host']
  -> file { '/etc/dsa/le-hook-dehydrated.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    content => inline_epp(@(EOF),
      <% |
         Stdlib::Fqdn $master_host,
      | -%>
      BASE="/srv/dehydrated/var/hook"
      MASTER="<%= $master_host %>"
      REBUILDZONES="sudo -u dnsadm -H /srv/dns.debian.org/bin/update --no-keys-update"
      | EOF
      {
        master_host => $trusted['certname'],
      }),
  }
  -> file { '/opt/dehydrated/hooks/dehydrated_hook':
    ensure => link,
    target => '/srv/letsencrypt.debian.org/repositories/letsencrypt-domains/bin/le-hook',
  }
}
