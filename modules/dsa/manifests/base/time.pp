# set up time source based on the system
class dsa::base::time {
  $localtimeservers = hiera('local-timeservers', [])
  $physicalHost = $deprecated::allnodeinfo[$fqdn]['physicalHost']
  $ntp_use_local_timeservers = hiera('ntp::use_local_timeservers', false)

  include dsa::base::ntp_purge

  if (!$ntp::use_local_timeservers and size($localtimeservers) > 0 and $::is_virtual and $::virtual == 'kvm') {
    include ntpsec::purge
    include systemdtimesyncd
  } else {
    include ntpsec
  }
}
