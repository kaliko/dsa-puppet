# = Class: samhain
#
# This class installs and configures samhain
#
#   include samhain
#
class dsa::base::samhain {
  ensure_packages(['samhain'])

  service { 'samhain':
    ensure    => running,
    hasstatus => false,
    pattern   => 'samhain',
    require   => Package['samhain'],
  }

  file { '/etc/samhain/samhainrc':
    content => template('dsa/base/samhain/samhainrc.erb'),
    require => Package['samhain'],
    notify  => Service['samhain']
  }

  file { '/etc/logrotate.d/samhain':
    content => template('dsa/base/samhain/logrotate.d-samhain'),
  }

  mon::service { 'procs/samhain-process': # {{{
    vars => {
      argument => '/usr/sbin/samhain',
      command  => 'samhain',
      user     => 'root',
      warning  => '1:8',
      critical => '1:',
    }
  } # }}}

  mon::service::shell_wrapped { 'samhain status': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/check_statusfile /var/cache/dsa/nagios/samhain',
  } # }}}

  mon::service { 'procs/samhain-process-zombies': # {{{
    vars => {
      argument => '/usr/sbin/samhain',
      command  => 'samhain',
      user     => 'root',
      warning  => '3',
      critical => '8',
      state    => 'Z',
    }
  } # }}}
}
