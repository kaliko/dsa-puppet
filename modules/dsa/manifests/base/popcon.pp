# Configure popcon on DSA hosts
class dsa::base::popcon {
  $popcon_host_id = hkdf('/etc/puppet/secret', "popcon_host_id-${trusted['certname']}")[0, 32]
  # slightly biased and leaking the host id, but meh
  $day = Integer("0x${popcon_host_id[0, 4]}") % 7

  ensure_packages(['popularity-contest'])
  file { '/etc/popularity-contest.conf':
    content => @("EOF"),
      ##
      ### THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
      ###
      PARTICIPATE="yes"
      USEHTTP="yes"
      MY_HOSTID="${popcon_host_id}"
      DAY="${day}"
      | EOF
  }
}
