# add a motd snippet
#
# @param order   pass to concat::fragment
# @param content pass to concat::fragment
#   Long lines are wrapped.
# @param content_raw pass to concat::fragment
# @param source  pass to concat::fragment
define dsa::base::motd::fragment (
  Any $order   = '100',
  Any $content = undef,
  Any $content_raw = undef,
  Any $source  = undef,
) {
  include dsa::base::motd

  concat::fragment { $name:
    target  => '/etc/motd',
    order   => $order,
    content => if $content { wrap($content) } elsif $content_raw { $content_raw },
    source  => $source,
  }
}

function wrap(String $in ) >> Any {
  ($in.split("\n").map |$x| {
    if ($x.length > 78) {
      $x.regsubst(/(.{1,78})( +|\Z)/, "\\1\n", 'G')
    } else {
      $x
    }
  } + ['']).join("\n")
}
