# Add haproxy monitoring/trending to munin
class dsa::munin_haproxy {
  file { '/usr/local/bin/munin-haproxyng':
    mode   => '0555',
    source => 'puppet:///modules/dsa/munin_haproxy/monitoring-munin-haproxy/haproxyng',
  }
  ensure_packages(['libswitch-perl'])
  file { '/etc/munin/plugin-conf.d/puppet-haproxyng':
    source => 'puppet:///modules/dsa/munin_haproxy/munin-haproxyng.conf',
  }
  munin::check { 'haproxyng':
    script => '../../../local/bin/munin-haproxyng',
  }
}
