# basic icinga checks
#
# @param kernel_tainted_ignore_bit
#   Ignore these bits in the kernel tainted check
class dsa::debmon (
  Optional[Variant[Integer,Array[Integer]]] $kernel_tainted_ignore_bit = undef,
) {
  mon::service { 'dsa_check_timedatectl': # {{{
    vars => {
      sync_only => if defined(Class['roles::broken_rtc']) { true } else { undef },
    },
  } # }}}
  mon::service { 'dsa_check_running_kernel/running_kernel': # {{{
    check_interval       => '60m',
    retry_interval       => '15m',
    create_check_command => true,
  }
  ensure_packages(['lz4', 'lzop']) # }}}
  mon::service { 'dsa_check_uptime/uptime': # {{{
    create_check_command => true,
  } # }}}
  mon::service { 'check_statusfile/apt-security-updates': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    vars           => {
      file => '/var/cache/dsa/nagios/apt',
    },
  #  vars           => {
  #    file => '/var/cache/nagios_status/apt',
  #  },
  #}
  #systemd::timer { 'noreply-icinga-update-apt-status.timer':
  #  enable          => true,
  #  active          => true,
  #  timer_content   => @(EOF),
  #    [Timer]
  #    OnBootSec=15m
  #    OnUnitActiveSec=45min
  #    RandomizedDelaySec=5m
  #    [Install]
  #    WantedBy=timers.target
  #    | EOF
  #  service_content => @("EOF"),
  #    [Unit]
  #    Description=update-apt-status for icinga
  #    Wants=network-online.target
  #    After=network-online.target
  #    [Service]
  #    Type=oneshot
  #    ExecStart=/usr/lib/nagios/cronjobs/update-apt-status
  #    | EOF
  #    # /
  } # }}}

  mon::service::shell_wrapped { 'puppet-environment': # {{{
    sudo    => 'root',  # to read puppet.conf
    command => @(EOF),
      environment=$(awk -F "=" '/^environment=/ {print $2}' /etc/puppet/puppet.conf)
      if [ "$environment" != "production" ]; then
        echo "Warning: Puppet environment $environment instead of production."
        exit 1
      fi
      echo "OK: Puppet environment check."
      exit 0
      | EOF
  } # }}}

  mon::service::shell_wrapped { 'systemd-status': # {{{
    command => '/bin/systemctl is-system-running',
  } # }}}
  mon::service::shell_wrapped { 'updated-libraries': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/check_libs',
    sudo           => true,
  } # }}}
  mon::service { 'dsa_check_config/setup-dsa-config': # {{{
    check_interval       => '60m',
    retry_interval       => '15m',
    create_check_command => true,
  } # }}}
  mon::service::shell_wrapped { 'setup-etc-hosts-hostname': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => @(EOF),
      if getent ahosts `hostname` | grep -q 127.0; then
        echo "Warning: local hostname resolves to 127/8 address"; exit 1;
      else
        echo "OK: Hostname resolves to non-127/8 address."; exit 0;
      fi
      | EOF
  } # }}}
  mon::service::shell_wrapped { 'memory-mb': # {{{
    command => '/usr/lib/nagios/plugins/dsa-check-memory -m mb',
  } # }}}
  mon::service::shell_wrapped { 'memory-percent': # {{{
    command => '/usr/lib/nagios/plugins/dsa-check-memory -m pct',
  } # }}}
  mon::service::shell_wrapped { 'entropy': # {{{
    command => '/usr/lib/nagios/plugins/dsa-check-entropy --watermark 256',
  } # }}}
  mon::service::shell_wrapped { 'filesystems': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/dsa-check-filesystems',
    sudo           => true,
  } # }}}
  mon::service::shell_wrapped { 'ssl-cert-host': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => @(EOF),
      if [ -e /etc/ssl/certs/thishost.pem ]; then
        /usr/lib/nagios/plugins/check_cert_expire /etc/ssl/certs/thishost.pem;
      else
        echo 'No thishost.pem on this host.';
      fi
      | EOF
      #/
  } # }}}
  mon::service::shell_wrapped { 'ssl-cert-host-debian-server': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => @(EOF),
      if [ -e /etc/ssl/debian/certs/thishost-server.crt ]; then
        /usr/lib/nagios/plugins/check_cert_expire /etc/ssl/debian/certs/thishost-server.crt;
      else
        echo 'No thishost-server.crt on this host.';
      fi
      | EOF
      #/
  } # }}}
  mon::service::shell_wrapped { 'ssl-cert-host-debian-client': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => @(EOF),
      if [ -e /etc/ssl/debian/certs/thishost.crt ]; then
        /usr/lib/nagios/plugins/check_cert_expire /etc/ssl/debian/certs/thishost.crt;
      else
        echo 'No thishost.crt on this host.';
      fi
      | EOF
      #/
  } # }}}
  mon::service::shell_wrapped { 'ssl-cert-host-puppet': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => "/usr/lib/nagios/plugins/check_cert_expire /var/lib/puppet/ssl/certs/${trusted['certname']}.pem",
    sudo           => true,
  } # }}}
  mon::service { 'check_kernel_tainted/kernel-tainted': # {{{
    vars => {
      ignore_bit => $kernel_tainted_ignore_bit,
    }
  } # }}}
  mon::service { 'check_fstab/fstab': # {{{
    create_check_command => true,
  } # }}}
  mon::service::shell_wrapped { 'local-resolver': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/dsa-check-resolver www.debian.org www.google.com',
  } # }}}

  mon::service { 'procs/procs-cron': # {{{
    vars => {
      argument => '/usr/sbin/cron',
      command  => 'cron',
      user     => 'root',
      warning  => '1:2',
      critical => '1:',
    }
  } # }}}
  mon::service { 'procs/procs-atd': # {{{
    vars => {
      argument => '/usr/sbin/atd',
      command  => 'atd',
      user     => 'daemon',
      warning  => '1:1',
      critical => '1:',
    }
  } # }}}
  mon::service { 'procs/procs-getty': # {{{
    vars => {
      argument => '/sbin/agetty',
      command  => 'agetty',
      user     => 'root',
      warning  => '1:8',
      critical => '1:',
    }
  } # }}}

  # unwanted processes {{{
  [
    'gkrellmd',
    'irqbalance',
    'openvpn',
    'snmpd',
    ['inetd', { ppid => 1,} ],
  ].each |$item| {
    if $item =~ String {
      $command = $item
      $extra = {}
    } else {
      [$command, $extra] = $item
    }
    mon::service { "procs/unwanted-process-${command}":
      check_interval => '60m',
      retry_interval => '15m',
      vars           => merge(
        {
          command => $command,
          warning => '0:0'
        },
        $extra
      ),
    }
  } # }}}

  # ignore bind mounts for disk usage check {{{
  mon::misc::add_info_fact { 'ignore-bindmountpoints':
    data => {
      'disk_all' => {
        'ignore' => {
          'partitions' => $facts['bindmountpoints'],
        },
      },
    },
  } # }}}

  include dsa::debmon::check_autofs
  include dsa::debmon::check_nfs_server
  unless $facts['is_virtual'] {
    include dsa::debmon::hardware
  }
}

# vim:set fdm=marker:
