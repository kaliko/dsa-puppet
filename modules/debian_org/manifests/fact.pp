# deploy a fact value
#
# @param fact_name   The name of the fact
# @param value       The value of the Fact
# @param fact_group  In which file to store the fact
define debian_org::fact (
  NotUndef $value,
  Any $fact_name = $name,
  Pattern[/\A[a-z0-9_-]*\z/] $fact_group = 'debian_org',
) {
  include debian_org

  $fn = "/etc/facter/facts.d/${fact_group}.json"
  ensure_resource('concat', $fn, {
    'owner'   => 'root',
    'group'   => 'root',
    'mode'    => '0400',
    'format'  => 'json-pretty',
  })
  concat::fragment { "debian_org::fact::${name}":
    target  => $fn,
    content => to_json({
      $fact_name => $value,
    }),
  }
}
