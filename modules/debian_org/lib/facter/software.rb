Facter.add("apache2") do
	setcode do
		if FileTest.exist?("/usr/sbin/apache2")
			true
		else
			false
		end
	end
end
Facter.add("clamd") do
	setcode do
		if FileTest.exist?("/usr/sbin/clamd")
			true
		else
			false
		end
	end
end
Facter.add("exim4") do
	setcode do
		if FileTest.exist?("/usr/sbin/exim4")
			true
		else
			false
		end
	end
end
Facter.add("exim4_ge_4_94") do
	setcode do
		system(%{test -n "$(dpkg-query -W -f='${Version}' exim4-base 2>/dev/null)" && dpkg --compare-versions "$(dpkg-query -W -f='${Version}' exim4-base)" ge 4.94})
	end
end
Facter.add("exim4_ge_4_96") do
	setcode do
		system(%{test -n "$(dpkg-query -W -f='${Version}' exim4-base 2>/dev/null)" && dpkg --compare-versions "$(dpkg-query -W -f='${Version}' exim4-base)" ge 4.96})
	end
end
Facter.add("postfix") do
	setcode do
		if FileTest.exist?("/usr/sbin/postfix")
			true
		else
			false
		end
	end
end
Facter.add("postgres") do
	setcode do
		pg = (FileTest.exist?("/usr/lib/postgresql/9.1/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/9.4/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/9.6/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/11/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/13/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/15/bin/postgres"))
		if pg
			true
		else
			false
		end
	end
end
Facter.add("postgrey") do
	setcode do
		if FileTest.exist?("/usr/sbin/postgrey")
			true
		else
			false
		end
	end
end
Facter.add("greylistd") do
	setcode do
		FileTest.exist?("/usr/sbin/greylistd")
	end
end
Facter.add("policydweight") do
	setcode do
		if FileTest.exist?("/usr/sbin/policyd-weight")
			true
		else
			false
		end
	end
end
Facter.add("spamd") do
	setcode do
		if FileTest.exist?("/usr/sbin/spamd")
			true
		else
			false
		end
	end
end
Facter.add("syslogversion") do
	setcode do
		%x{dpkg-query -W -f='${Version}\n' syslog-ng | cut -d. -f1-2}.chomp
	end
end
Facter.add("unbound") do
	unbound=(FileTest.exist?("/usr/sbin/unbound") and
		FileTest.exist?("/var/lib/unbound/root.key"))
	setcode do
		if unbound
			true
		else
			false
		end
	end
end
Facter.add("dsa_systemd_ge_245_4_2") do
	setcode do
		system(%{test -n "$(dpkg-query -W -f='${Version}' systemd 2>/dev/null)" && dpkg --compare-versions "$(dpkg-query -W -f='${Version}' systemd)" ge 245.4-2})
	end
end
Facter.add("update_grub") do
	setcode do
		FileTest.exist?("/usr/sbin/update-grub")
	end
end
Facter.add("haproxy") do
	setcode do
		FileTest.exist?("/usr/sbin/haproxy")
	end
end
Facter.add("smartd") do
	setcode do
		FileTest.exist?("/usr/sbin/smartd")
	end
end
Facter.add("etckeeper") do
	setcode do
		FileTest.exist?("/usr/bin/etckeeper")
	end
end
Facter.add("srsd") do
	setcode do
		FileTest.exist?("/usr/bin/srsd")
	end
end
