# add an exim dkim domain entry on this host
#
# @param domain     email domain (defaults to $name)
define exim::dkimdomain (
  String $domain = $name,
) {
  concat::fragment { "dkim_${domain}":
    target  => '/etc/exim4/dkim-domains',
    content => $domain,
  }

  dnsextras::dkim::cname { $domain : }
}
