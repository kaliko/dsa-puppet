# add an exim virtualdomain entry on this host
#
# @param mail_user  User to run the virtual email domain's pipe jobs and deliveries as
# @param mail_group Group to run the virtual email domain's pipe jobs and deliveries as
# @param owner      User to own the directories and files
# @param group      Group to own the directories and files
# @param domain     email domain (defaults to $name)
# @param maildir    Mail directory, usually and defaults to /srv/$name/mail
define exim::vdomain (
  String $domain = $name,
  String $maildir = "/srv/${name}/mail",
  String $owner = 'root',
  String $group = 'root',
  String $mail_user = $owner,
  String $mail_group = $group,
) {
  file { $maildir:
    ensure => directory,
    mode   => '2755',
    owner  => $owner,
    group  => $group,
  }

  file { [
    "${maildir}/aliases",
    "${maildir}/callout_users",
    "${maildir}/grey_users",
    "${maildir}/neversenders",
    "${maildir}/rbllist",
    "${maildir}/rhsbllist",
    ] :
    mode  => '0644',
    owner => $owner,
    group => $group,
  }

  concat::fragment { "virtualdomain_${domain}":
    target  => '/etc/exim4/virtualdomains',
    content => "${domain}: user=${mail_user} group=${mail_group} directory=${maildir}\n",
  }
}
