class roles::keyring {
  include apache2

  rsync::site { 'keyring':
    source  => 'puppet:///modules/roles/keyring/rsyncd.conf',
    sslname => 'keyring.debian.org',
  }

  ssl::service { 'keyring.debian.org':
    notify   => Exec['service apache2 reload'],
    tlsaport => [443, 1873],
  }

  include named::authoritative

  $notify_address_bind = join(getfromhash($deprecated::allnodeinfo, 'denis.debian.org', 'ipHostNumber'), '; ')

  ferm::rule::simple { 'keyserver':
    port => 11371
  }

  Ferm::Rule::Simple <<| tag == 'named::keyring::ferm' |>>

  concat::fragment { 'dsa-named-conf-puppet-misc---openpgpkey-zone':
    target  => '/etc/bind/named.conf.puppet-misc',
    order   => '020',
    content => @("EOF"),
      zone "_openpgpkey.debian.org" {
        type master;
        file "/srv/keyring.debian.org/_openpgpkey.debian.org.zone";
        allow-query { any; };
        allow-transfer {
          key tsig-denis.debian.org-kaufmann.debian.org ;
          127.0.0.1;
        };
        also-notify {
          ${notify_address_bind};
        };
      };
      | EOF
  }

}
