class roles::lists {
  include apache2

  ssl::service { 'lists.debian.org':
    notify => Exec['service apache2 reload'],
  }

  ferm::rule::simple { 'dsa-smtp':
    description => 'Allow smtp access from the world',
    port        => '25',
  }
}
