class roles::dgit_browse {
  include apache2
  include roles::dgit_sync_target

  ssl::service { 'browse.dgit.debian.org':
    notify => Exec['service apache2 reload'],
  }

  package { 'cgit': ensure => installed, }
  package { 'python3-pygments': ensure => installed, }
  package { 'python3-chardet': ensure => installed, }

  file { '/etc/cgitrc':
    source => 'puppet:///modules/roles/dgit/cgitrc',
  }
  file { '/etc/apache2/conf-enabled/cgit.conf':
    ensure => absent,
    notify => Exec['service apache2 reload'],
  }

  apache2::site { '010-browse.dgit.debian.org':
    site   => 'browse.dgit.debian.org',
    source => 'puppet:///modules/roles/dgit/browse.dgit.debian.org',
  }
}
