# the geodns noes
class roles::dns_geodns {
  include named::geodns

  ssh::keygen { 'geodnssync': }
  ssh::authorized_key_add { 'dns_geodns::pull-from-primary':
    target_user => 'geodnssync',
    command     => '/usr/bin/rsync --server --sender -logDtprze.iL . zonefiles/',
    key         => $facts['geodnssync_key'],
    collect_tag => 'dns_primary',
  }

  ssh::authorized_key_collect { 'geodnssync-node':
    target_user => 'geodnssync',
    collect_tag => 'geodnssync-node',
  }

  @@ferm::rule::simple { "dsa-bind-from-${::fqdn}":
    tag         => 'named::primary::ferm',
    description => 'Allow geo nameserver access to the primary for the (non-geo) zones that we AXFR',
    proto       => ['udp', 'tcp'],
    port        => 'domain',
    saddr       => $base::public_addresses,
  }
}
