# a syncproxy
# @param syncproxy_name  the service name of this syncproxy
# @param listen_addr IP addresses to have rsync and apache listen on, and ssh to trigger from
class roles::syncproxy(
  String $syncproxy_name,
  Array[Stdlib::IP::Address] $listen_addr = [],
) {
  include roles::archvsync_base

  $enclosed_addresses_rsync = empty($listen_addr) ? {
    true    => ['[::]'],
    default => enclose_ipv6($listen_addr),
  }
  $enclosed_addresses_apache = empty($listen_addr) ? {
    true    => ['*'],
    default => enclose_ipv6($listen_addr),
  }
  $ssh_source_addresses = empty($listen_addr) ? {
    true    => $base::public_addresses,
    default => $listen_addr,
  }

  $mirror_basedir_prefix = hiera('role_config__syncproxy.mirror_basedir_prefix')

  file { '/etc/rsyncd':
    ensure => 'directory'
  }

  file { '/etc/rsyncd/debian.secrets':
    owner => 'root',
    group => 'mirroradm',
    mode  => '0660',
  }

  include apache2
  include apache2::ssl
  ssl::service { $syncproxy_name:
    notify => Exec['service apache2 reload'],
  }
  apache2::site { '010-syncproxy.debian.org':
    site    => 'syncproxy.debian.org',
    content => template('roles/syncproxy/syncproxy.debian.org-apache.erb')
  }

  file { [ '/srv/www/syncproxy.debian.org', '/srv/www/syncproxy.debian.org/htdocs' ]:
    ensure => directory,
    mode   => '0755',
  }
  file { '/srv/www/syncproxy.debian.org/htdocs/index.html':
    content => template('roles/syncproxy/syncproxy.debian.org-index.html.erb')
  }

  rsync::site { 'syncproxy':
    content => template('roles/syncproxy/rsyncd.conf.erb'),
    binds   => $enclosed_addresses_rsync,
    sslname => $syncproxy_name,
  }


  # ssh firewalling setup
  ###
  @@ferm::rule::simple { "dsa-ssh-from-syncproxy-${::fqdn}":
    tag         => 'ssh::server::from::syncproxy',
    description => 'Allow ssh access from a syncproxy',
    chain       => 'ssh',
    saddr       => $ssh_source_addresses,
  }
  # syncproxies should be accessible from various role hosts
  Ferm::Rule::Simple <<|
    tag == 'ssh::server::from::syncproxy' or
    tag == 'ssh::server::from::ftp_master' or
    tag == 'ssh::server::from::ports_master' or
    tag == 'ssh::server::from::security_master'
    |>>
}
