class roles::piuparts_runner {
  package { 'debian.org-piuparts-slave.debian.org': ensure => installed, }

  file { [
      '/srv/piuparts.debian.org',
      '/srv/piuparts.debian.org/home-slave'
    ]:
      ensure => directory,
      mode   => '0755',
      owner  => 'piupartss',
      group  => 'piuparts',
  }
  file { '/home/piupartss':
    ensure => link,
    target => '/srv/piuparts.debian.org/home-slave',
  }

  file { '/etc/piuparts':
    ensure => link,
    target => '/srv/piuparts.debian.org/etc/piuparts',
  }

  ssh::keygen { 'piupartss': }
  ssh::authorized_key_add { 'piuparts_runner::piuparts':
    target_user => 'piupartsm',
    collect_tag => 'roles::piuparts_runner::to::piuparts',
    command     => '/srv/piuparts.debian.org/share/piuparts/piuparts-master',
    key         => dig($facts, 'ssh_keys_users', 'piupartss', 'id_rsa.pub', 'line'),
  }
}
