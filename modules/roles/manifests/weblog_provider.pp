# a provider of webserver logs
class roles::weblog_provider {
  ssh::keygen {'weblogsync': }
  ssh::authorized_key_add { 'weblogsync-provider::destination':
    target_user => 'weblogsync',
    key         => dig($facts, 'ssh_keys_users', 'weblogsync', 'id_rsa.pub', 'line'),
    command     => "/srv/weblogs.debian.org/bin/ssh-wrap ${::fqdn}",
    collect_tag => 'weblogsync',
  }

  file { '/etc/cron.d/puppet-weblog-provider': ensure => absent, }
  concat::fragment { 'puppet-crontab--weblog-provider':
    target => '/etc/cron.d/puppet-crontab',
    content  => @(EOF)
                0 1 * * * weblogsync sleep $((RANDOM \% 1800)); rsync -a --delete-excluded --include 'www.debian.org-access.log-*gz' --include '*-public-access.log-*gz' --exclude '**' /var/log/apache2/. weblogsync@wolkenstein.debian.org:-weblogs-incoming-
                | EOF
  }
}
