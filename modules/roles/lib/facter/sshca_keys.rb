require 'etc'

# This fact will collect the valid CA keys from the SSH CA running as user
# sshca.
Facter.add(:sshca_keys) do
  confine { Etc.getpwnam('sshca') rescue nil }

  setcode do
    Dir.glob(File.join(Etc.getpwnam('sshca').dir, 'ca', '*.pub')).collect { |filepath|
      File.read(filepath).strip
    }
  end
end

