# postfix class
# @param use_smarthost use the smarthost
# @param smarthost host to relay through (if set and use_smarthost)
# @param manage_maincf whether the main configuation file is managed
class postfix(
  Optional[String] $smarthost,
  Boolean $use_smarthost = true,
  Boolean $manage_maincf = true,
) {
  package { 'postfix':
    ensure => installed
  }

  if $use_smarthost {
    if ! smarthost {
      fail('No smarthost set but use_smarthost is true')
    }
  } else {
    $heavy = true
  }

  service { 'postfix':
    ensure => running
  }

  munin::check { 'ps_exim4':       ensure => absent }
  munin::check { 'exim_mailqueue': ensure => absent }
  munin::check { 'exim_mailstats': ensure => absent }

  munin::check { 'postfix_mailqueue': }
  munin::check { 'postfix_mailstats': }
  munin::check { 'postfix_mailvolume': }
  munin::check { 'ps_smtp': script => 'ps_' }
  munin::check { 'ps_smtpd': script => 'ps_' }

  if $manage_maincf {
    concat { '/etc/postfix/main.cf':
      notify  => Exec['service postfix reload'],
    }
    concat::fragment { 'puppet-postfix-main.cf--header':
      target  => '/etc/postfix/main.cf',
      order   => '000',
      content => template('postfix/main.cf-header.erb')
    }
  }
  if $heavy {
    include clamav
    include fail2ban::postfix
  }

  exec { 'service postfix reload':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => 'service postfix reload',
    refreshonly => true,
    require     =>  Package['postfix'],
  }
}
