#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# vim:set et ts=4 sw=4:
# Copyright © 2010 Marc Brockschmidt <he@debian.org>
# Copyright © 2010 Andreas Barth <aba@not.so.argh.org>
# Copyright © 2010 Philipp Kern <pkern@debian.org>
#
# buildd is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# buildd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
#####################################################################

set -e
shopt -s extglob

[ "$CHROOT_PROFILE" = "buildd" ] || exit 0

. "$SETUP_DATA_DIR/common-data"
. "$SETUP_DATA_DIR/common-functions"
. "$SETUP_DATA_DIR/common-config"

if [ "$CHROOT_SESSION_SOURCE" = "false" ] ; then
    CHROOT_ALIAS="${CHROOT_ALIAS/#experimental-/sid-experimental-}"
    case $CHROOT_ALIAS in
        +([^-])-+([^-])-hurd-*-sbuild*)
        SUITE_BASE=$(echo "$CHROOT_ALIAS" | cut -f1 -d-)
        SUITE_VARIANT=$(echo "$CHROOT_ALIAS" | cut -f2 -d-)
        ;;
        *-*-*-*-sbuild*)
        SUITE_BASE=$(echo "$CHROOT_ALIAS" | cut -f1 -d-)
        SUITE_VARIANT=$(echo "$CHROOT_ALIAS" | cut -f2,3 -d-)
        ;;
        +([^-])-hurd-*-sbuild*)
        SUITE_BASE=$(echo "$CHROOT_ALIAS" | cut -f1 -d-)
        if [ "${SUITE_BASE}" != "sid" ]; then
            SUITE_VARIANT="proposed-updates"
        fi
        ;;
        *-*-*-sbuild*)
        SUITE_BASE=$(echo "$CHROOT_ALIAS" | cut -f1 -d-)
        SUITE_VARIANT=$(echo "$CHROOT_ALIAS" | cut -f2 -d-)
        ;;
        *-*-sbuild*)
        SUITE_BASE=$(echo "$CHROOT_ALIAS" | cut -f1 -d-)
        if [ "${SUITE_BASE}" != "sid" ]; then
            SUITE_VARIANT="proposed-updates"
        fi
        ;;
    esac
fi

if [ "${SUITE_BASE}" != "buster" ] ; then
    SUITE_SECURITY=-security
else
    SUITE_SECURITY=/updates
fi

VERBOSE=""
if [ "$AUTH_VERBOSITY" = "verbose" ]; then
      VERBOSE="--verbose"
fi

if [ -f /etc/schroot/dsa/default-mirror ]; then
    debian_mirror=$(cat /etc/schroot/dsa/default-mirror)
    if [ "${debian_mirror%/}" = "https://deb.debian.org/debian" ]; then
        debian_mirror=""
    fi
elif [ -f /etc/schroot/conf.buildd ]; then
    . /etc/schroot/conf.buildd
fi

function domirror() {
    for SUITE in ${SUITES:-SUITE}; do
        echo deb "$1"     | sed -e "s,COMPONENT,${COMPONENT},"     -e "s,SUITE,${SUITE}," >> "$2"
        echo deb-src "$1" | sed -e "s,COMPONENT,${COMPONENT_SRC}," -e "s,SUITE,${SUITE}," >> "$2"
    done
}

if [ "$1" = "setup-start" ] || [ "$1" = "setup-recover" ]; then
    if [ -n "${SUITE_BASE}" ] ; then
        APT_LIST="${CHROOT_PATH}/etc/apt/sources.buildd.list"
        APT_CONF="${CHROOT_PATH}/etc/apt/apt.conf.d/99buildd.conf"
        APT_KEYS=""
        rm -f "${APT_LIST}" "${APT_CONF}"

        COMPONENT="main contrib"
        COMPONENT_SRC="main contrib non-free"
        if [ "${SUITE_BASE}" != "buster" ] && [ "${SUITE_BASE}" != "bullseye" ] ; then
            COMPONENT_SRC="${COMPONENT_SRC} non-free-firmware"
        fi

        if [ "${SUITE_VARIANT}" = 'proposed-updates' ]; then
            SUITES="${SUITE_BASE} ${SUITE_BASE}-proposed-updates"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" "${APT_LIST}"
            domirror "https://deb.debian.org/debian SUITE COMPONENT" "${APT_LIST}"
            if [ "${debian_incoming}" != 'no' ]; then
                SUITES="${SUITE_BASE}-proposed-updates"
                domirror "https://incoming.debian.org/debian-buildd buildd-SUITE COMPONENT" "${APT_LIST}"
            fi

        elif [ "${SUITE_VARIANT}" = 'security' ]; then
            SUITES="${SUITE_BASE}"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} ${SUITE_BASE} COMPONENT" "${APT_LIST}"
            domirror "https://deb.debian.org/debian ${SUITE_BASE} COMPONENT" "${APT_LIST}"

            [ -n "${security_mirror}" ] && domirror "${security_mirror} ${SUITE_BASE}${SUITE_SECURITY} COMPONENT" "${APT_LIST}"
            domirror "https://security-master.debian.org/debian-security ${SUITE_BASE}${SUITE_SECURITY} COMPONENT" "${APT_LIST}"
            domirror "https://security-master.debian.org/debian-security-buildd buildd-${SUITE_BASE}${SUITE_SECURITY} COMPONENT" "${APT_LIST}"

            # Temporarily add proposed-updates for bullseye-security and bookworm-security chroots
            # for e.g. openjdk-11 and firefox-esr/thunderbird updates (see RT#9257)
            if [ "${SUITE_BASE}" = "bullseye" ] || [ "${SUITE_BASE}" = "bookworm" ] ; then
                SUITES="${SUITE_BASE}-proposed-updates"
                [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" ${APT_LIST}
                domirror "https://deb.debian.org/debian SUITE COMPONENT" ${APT_LIST}
            fi

        elif [ "${SUITE_VARIANT%%-sloppy}" = 'backports' ]; then
            SUITES="${SUITE_BASE}"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" "${APT_LIST}"
            domirror "https://deb.debian.org/debian SUITE COMPONENT" "${APT_LIST}"
            SUITES="${SUITE_BASE}"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE-backports COMPONENT" "${APT_LIST}"
            domirror "https://deb.debian.org/debian SUITE-backports COMPONENT" "${APT_LIST}"
            if [ "${debian_incoming}" != 'no' ]; then
                domirror "https://incoming.debian.org/debian-buildd buildd-SUITE-backports COMPONENT" "${APT_LIST}"
            fi
            if [ "$(echo ${SUITE_VARIANT} | cut -d - -f 2)" = "sloppy" ]; then
                [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE-backports-sloppy COMPONENT" "${APT_LIST}"
                domirror "https://deb.debian.org/debian SUITE-backports-sloppy COMPONENT" "${APT_LIST}"
                if [ "${debian_incoming}" != 'no' ]; then
                    domirror "https://incoming.debian.org/debian-buildd buildd-SUITE-backports-sloppy COMPONENT" "${APT_LIST}"
                fi
            fi

        elif [ "${SUITE_BASE}" = 'sid' ]; then
            SUITES="unstable"
            if [ "${SUITE_VARIANT}" = "experimental" ]; then
                SUITES="unstable experimental"
            fi
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" "${APT_LIST}"
            domirror "https://deb.debian.org/debian SUITE COMPONENT" "${APT_LIST}"
            if [ "${debian_incoming}" != 'no' ]; then
                domirror "https://incoming.debian.org/debian-buildd buildd-SUITE COMPONENT" "${APT_LIST}"
            fi

        else
            echo "ERROR: cannot adjust sources.list: ${SUITE_BASE}/${SUITE_VARIANT} unknown - exiting"
            exit 1

        fi
        echo 'Acquire::PDiffs "false";' >> "${APT_CONF}"
        echo 'APT::Install-Recommends 0;' >> "${APT_CONF}"
        echo 'Dir::Etc::SourceList "sources.buildd.list";' >> "${APT_CONF}"
        echo 'Acquire::Languages "none";' >> "${APT_CONF}"

        if [ -n "${APT_KEYS}" ]; then
            for KEY in ${APT_KEYS}; do
                if [ -f "/usr/share/buildd/${KEY}.asc" ]; then
                    chroot "${CHROOT_PATH}" apt-key add - < "/usr/share/buildd/${KEY}.asc" >&2
                else
                    echo "W: should add apt key ${KEY} but not found" >&2
                fi
            done
        fi
    else
    if [ -n "$SUITE_VARIANT" ]; then
        SOURCES_FILE_NAME="${SUITE_BASE}-${SUITE_VARIANT}.sources"
        SOURCES_FILE_PATH="/etc/schroot/buildd.d/${SOURCES_FILE_NAME}"
        APT_LIST_AUTO="${CHROOT_PATH}/etc/apt/sources.list.d/auto.list"

        if [ -f "$SOURCES_FILE_PATH" ]; then
                cp $VERBOSE "${SOURCES_FILE_PATH}" "${CHROOT_PATH}/etc/apt/sources.list.d/${SOURCES_FILE_NAME}.list"
                elif [ ${SUITE_VARIANT} = 'security' ]; then
                    : > "${APT_LIST_AUTO}"
                    if [ -n "${security_mirror}" ]; then
                        echo "deb ${security_mirror} ${SUITE_BASE}${SUITE_SECURITY} main contrib" >> "${APT_LIST_AUTO}"
                        echo "deb-src ${security_mirror} ${SUITE_BASE}${SUITE_SECURITY} main contrib" >> "${APT_LIST_AUTO}"
                    fi
                    echo "deb https://security-master.debian.org/debian-security ${SUITE_BASE}${SUITE_SECURITY} main contrib" >> "${APT_LIST_AUTO}"
                    echo "deb-src https://security-master.debian.org/debian-security ${SUITE_BASE}${SUITE_SECURITY} main contrib" >> "${APT_LIST_AUTO}"
                    echo "deb https://security-master.debian.org/debian-security-buildd buildd-${SUITE_BASE}${SUITE_SECURITY} main contrib" >> "${APT_LIST_AUTO}"
                    echo "deb-src https://security-master.debian.org/debian-security-buildd buildd-${SUITE_BASE}${SUITE_SECURITY} main contrib" >> "${APT_LIST_AUTO}"
                elif [ ${SUITE_VARIANT%%-sloppy} = 'backports' ]; then
                    : > "${APT_LIST_AUTO}"
                    if [ -n "${debian_mirror}" ]; then
                        echo "deb ${debian_mirror} ${SUITE_BASE}-backports main contrib" >> "${APT_LIST_AUTO}"
                        echo "deb-src ${debian_mirror} ${SUITE_BASE}-backports main contrib non-free" >> "${APT_LIST_AUTO}"
                    fi
                    echo "deb https://deb.debian.org/debian ${SUITE_BASE}-backports main contrib" >> "${APT_LIST_AUTO}"
                    echo "deb-src https://deb.debian.org/debian ${SUITE_BASE}-backports main contrib non-free" >> "${APT_LIST_AUTO}"
                    echo "deb https://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports main contrib"  >> "${APT_LIST_AUTO}"
                    echo "deb-src https://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports main contrib non-free" >> "${APT_LIST_AUTO}"
                    if [ "$(echo ${SUITE_VARIANT} | cut -d - -f 2)" = "sloppy" ]; then
                        if [ -n "${debian_mirror}" ]; then
                            echo "deb ${debian_mirror} ${SUITE_BASE}-backports-sloppy main contrib" >> "${APT_LIST_AUTO}"
                            echo "deb-src ${debian_mirror} ${SUITE_BASE}-backports-sloppy main contrib non-free" >> "${APT_LIST_AUTO}"
                        fi
                        echo "deb https://deb.debian.org/debian ${SUITE_BASE}-backports-sloppy main contrib" >> "${APT_LIST_AUTO}"
                        echo "deb-src https://deb.debian.org/debian ${SUITE_BASE}-backports-sloppy main contrib non-free" >> "${APT_LIST_AUTO}"
                        echo "deb https://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports-sloppy main contrib"  >> "${APT_LIST_AUTO}"
                        echo "deb-src https://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports-sloppy main contrib non-free" >> "${APT_LIST_AUTO}"
                    fi
                elif [ "${SUITE_VARIANT}" = 'experimental' ]; then
                    : > "${APT_LIST_AUTO}"
                    if [ -n "${debian_mirror}" ]; then
                        echo "deb ${debian_mirror} experimental main contrib" >> "${APT_LIST_AUTO}"
                        echo "deb-src ${debian_mirror} experimental main contrib non-free non-free-firmware" >> "${APT_LIST_AUTO}"
                    fi
                    echo "deb     https://deb.debian.org/debian experimental main contrib" >> "${APT_LIST_AUTO}"
                    echo "deb-src https://deb.debian.org/debian experimental main contrib non-free non-free-firmware" >> "${APT_LIST_AUTO}"
                    echo "deb     https://incoming.debian.org/debian-buildd buildd-experimental main contrib" >> "${APT_LIST_AUTO}"
                    echo "deb-src https://incoming.debian.org/debian-buildd buildd-experimental main contrib non-free non-free-firmware" >> "${APT_LIST_AUTO}"
        fi
    fi
    fi
elif [ "$1" = "setup-stop" ]; then
    if [ -n "${SUITE_BASE}" ] && ( [ -f /etc/schroot/conf.buildd ] || [ -f /etc/schroot/dsa/default-mirror ] ); then
        APT_LIST="${CHROOT_PATH}/etc/apt/sources.buildd.list"
        APT_CONF="${CHROOT_PATH}/etc/apt/apt.conf.d/99buildd.conf"
        rm -f "${APT_LIST}" "${APT_CONF}"
    else
    if [ -n "$SUITE_VARIANT" ]; then
        SOURCES_FILE_NAME="${SUITE_BASE}-${SUITE_VARIANT}.sources"
        SOURCES_FILE_PATH="/etc/schroot/buildd.d/${SOURCES_FILE_NAME}"

        if [ -f "$SOURCES_FILE_PATH" ]; then
                rm -f $VERBOSE "${CHROOT_PATH}/etc/apt/sources.list.d/${SOURCES_FILE_NAME}.list"
                elif [ ${SUITE_VARIANT} = 'security' ] || [ ${SUITE_VARIANT} = 'backports' ] || [ ${SUITE_VARIANT} = 'experimental' ] ; then
                rm -f $VERBOSE "${CHROOT_PATH}/etc/apt/sources.list.d/auto.list"
        fi
    fi
    fi
fi
