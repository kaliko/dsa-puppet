#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# by weasel

set -e

[ "$CHROOT_PROFILE" = "dsa" ] || [ "$CHROOT_PROFILE" = "buildd-dsa" ] || exit 0

. "$SETUP_DATA_DIR/common-data"
. "$SETUP_DATA_DIR/common-functions"

if [ -f "${CHROOT_SCRIPT_CONFIG:-}" ]; then
    . "$CHROOT_SCRIPT_CONFIG"
elif [ -f "$CHROOT_PROFILE_DIR/config" ]; then
    . "$CHROOT_PROFILE_DIR/config"
else
    fatal "Cannot find config script"
fi

suite_alias="$SUITE_BASE"

if [ "$1" = "setup-start" ] || [ "$1" = "setup-recover" ]; then
  SRCL="${CHROOT_PATH}/etc/apt/sources.list.d/auto.list"
  rm -f "$SRCL"
  mirror=${MIRROR:-https://deb.debian.org/debian}
  debugmirror=${DEBUGMIRROR:-https://deb.debian.org/debian-debug/}
  securitydebugmirror=${SECURITYDEBUGMIRROR:-https://deb.debian.org/debian-security-debug/}

  case "$SUITE_ARCH" in
    ppc64)
      case "${SUITE_BASE:-}" in
        experimental)
          echo "deb     https://deb.debian.org/debian-ports experimental main" >> "$SRCL"
          echo "deb-src $mirror experimental main" >> "$SRCL"

          echo "deb     https://deb.debian.org/debian-ports sid main" >> "$SRCL"
          echo "deb-src $mirror sid main" >> "$SRCL"

          echo "deb     https://deb.debian.org/debian-ports unreleased main" >> "$SRCL"
          echo "deb-src https://deb.debian.org/debian-ports unreleased main" >> "$SRCL"
          ;;
        sid)
          echo "deb     https://deb.debian.org/debian-ports sid main" >> "$SRCL"
          echo "deb-src $mirror sid main" >> "$SRCL"

          echo "deb     https://deb.debian.org/debian-ports unreleased main" >> "$SRCL"
          echo "deb-src https://deb.debian.org/debian-ports unreleased main" >> "$SRCL"
          ;;
        *)
          fatal "Unexpected suite base $SUITE_BASE"
          ;;
      esac
      ;;
    *)
      case "${SUITE_BASE:-}" in
        experimental)
          echo "deb     $mirror sid main" >> "$SRCL"
          echo "deb-src $mirror sid main" >> "$SRCL"

          echo "deb     $debugmirror sid-debug main" >> "$SRCL"
          echo "deb-src $debugmirror sid-debug main" >> "$SRCL"
          ;;
        sid)
          ;;
        buster)
          echo "deb     https://deb.debian.org/debian-security ${suite_alias}/updates main" >> "$SRCL"
          echo "deb-src https://deb.debian.org/debian-security ${suite_alias}/updates main" >> "$SRCL"

          echo "deb     $securitydebugmirror ${suite_alias}-debug/updates main" >> "$SRCL"
          echo "deb-src $securitydebugmirror ${suite_alias}-debug/updates main" >> "$SRCL"
          ;;
        *)
          echo "deb     https://deb.debian.org/debian-security ${suite_alias}-security main" >> "$SRCL"
          echo "deb-src https://deb.debian.org/debian-security ${suite_alias}-security main" >> "$SRCL"

          echo "deb     $securitydebugmirror ${suite_alias}-security-debug main" >> "$SRCL"
          echo "deb-src $securitydebugmirror ${suite_alias}-security-debug main" >> "$SRCL"
          ;;
      esac

      echo "deb     $debugmirror ${suite_alias}-debug main" >> "$SRCL"
      echo "deb-src $debugmirror ${suite_alias}-debug main" >> "$SRCL"

      echo "deb     $mirror ${suite_alias} main" >> "$SRCL"
      echo "deb-src $mirror ${suite_alias} main" >> "$SRCL"

      case "${SUITE_VARIANT:-}" in
        backports)
          echo "deb     $mirror ${SUITE_BASE}-${SUITE_VARIANT} main" >> "$SRCL"
          echo "deb-src $mirror ${SUITE_BASE}-${SUITE_VARIANT} main" >> "$SRCL"
          ;;
      esac
      ;;
  esac
  echo "o To install build dependencies run"
  echo "  dd-schroot-cmd -c ${SESSION_ID} apt-get update"
  echo "  followed by build-dep/install as appropriate in the host system."
  echo "o If you started this session with schroot -b, please do not forget to run"
  echo "  schroot --end-session -c ${SESSION_ID}"
  echo "  when you no longer need this environment."
fi
