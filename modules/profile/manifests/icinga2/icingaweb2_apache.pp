# Configure icinga2 server
#
class profile::icinga2::icingaweb2_apache (
) {
  assert_private()

  $digestfile = '/etc/apache2/mon-htpasswd'

  class { 'apache2':
    mpm => 'prefork',
  }

  include apache2::auth_digest
  include apache2::authn_file
  include apache2::module::authn_anon
  include apache2::rewrite
  include apache2::ssl

  file { '/etc/apache2/conf-enabled/icingaweb2.conf':
    ensure => absent,
    notify => Exec['service apache2 reload'],
  }
  file { $digestfile:
    ensure => file,
    owner  => 'root',
    group  => 'www-data',
    mode   => '0640',
  }
  ssl::service { 'mon.debian.org':
    notify => Exec['service apache2 reload'],
  }
  onion::service { 'mon.debian.org': port => 80, target_address => 'mon.debian.org', target_port => 80, direct => true }
  apache2::site { 'mon.debian.org':
    content => template('profile/icinga2/apache-mon.debian.org.conf.erb')
  }

  ensure_packages([
  'php-curl',
  'php-pgsql',
  ],
  {
    notify => Service['apache2'],
  })
}
