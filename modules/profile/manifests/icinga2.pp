# Configure and enable icinga2
#
# Configures icinga on both the central monitoring host as well as on
# each individual node (satellite) that gets monitored.
#
# @param admin_users
#  List of users to give admin access in icingaweb2
class profile::icinga2 (
  Array[String] $admin_users = []
) {
  $master = 'debmon-01.debian.org'
  if $trusted['certname'] == $master {
    # set up apache
    include profile::icinga2::icingaweb2_apache

    # set up lvm for pg
    lvm::logical_volume{ 'var-lib-postgresql':
      ensure       => present,
      volume_group => "vg_${trusted['hostname']}",
      initial_size => '20G',
      fs_type      => 'ext4',
      mountpath    => '/var/lib/postgresql',
    }

    class { 'postgresql::globals':
      version => '13',
    }

    $require = [ Lvm::Logical_volume['var-lib-postgresql'] ]
    $extra = {
      icinga2_ido_db_password        => hkdf('/etc/puppet/secret', "${name}::${trusted['certname']}::ido_db_password"),
      icinga2_icingaweb_api_password => hkdf('/etc/puppet/secret', "${name}::${trusted['certname']}::icingaweb_api_password"),
      icinga2_icingaweb_roles        => {
        'admin-role' => {
          users       => $admin_users.join(','),
          permissions => '*',
        }
      }
    }

    class { 'profile::icinga2::kgb': }

    ferm::rule { 'dsa-icinga2':
      description => 'Allow icinga2 access',
      domain      => '(ip ip6)',
      rule        => '&SERVICE_RANGE(tcp, 5665, $HOST_DEBIAN)',
    }
  } else {
    $require = []
    $extra = {}
  }

  class { 'mon':
    require                            => $require,

    icinga2_masters                    => [$master],
    icinga2_icinga_ca_host_fingerprint => '12:3D:CA:76:AA:B9:81:09:DE:E0:B2:83:7F:A3:6B:46:83:EB:65:6F:26:1D:FC:CA:34:41:E5:44:52:D4:F0:16',
    icinga2_ssl_ticket_salt            => hkdf('/etc/puppet/secret', "${name}::${master}::ticketsalt"),
    firewall                           => 'ferm',
    checkcommand_classname_prefixes    => [ 'dsa::debmon::check_commands' ],
    checkcommand_sources               => [ 'puppet:///modules/dsa/debmon/check_commands' ],
    *                                  => $extra,
  }

  if $trusted['certname'] == $master {
    # we want to monitor ssh
    @@ferm::rule::simple { "dsa-ssh-from-nagios-${::fqdn}":
      tag         => 'ssh::server::from::nagios',
      description => 'Allow ssh access from the nagios server',
      chain       => 'ssh',
      saddr       => $base::public_addresses,
    }
  }

  class { 'dsa::debmon': }
}
